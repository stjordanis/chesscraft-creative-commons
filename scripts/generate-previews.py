"""
This script generates a PNG which is a grid of many random pieces.
"""

import os, random
from PIL import Image

def get_matching_paths_recursively(rootdir, extension, verbose=0):
    "Returns a list of full paths. Searches 'rootdir' recursively for all files with 'extension'."
    if verbose:
        print("Getting filenames for files with extension \"%s\" at:\n%s" %
              (extension, rootdir))
    results = []
    for root, _, filenames in os.walk(rootdir):
        for filename in filenames:
            if filename.find(extension) == len(filename) - len(extension):
                path = os.path.join(root, filename)
                results.append(path)
    return results

def filter_paths(paths):
    blacklist=("01_classic","06_classic2","07_berserker")
    for black in blacklist:
        paths=[path for path in paths if black not in path]
    return paths

def get_pieces_dictionary(source_folder):
    "pieces = {'pawn.png' : ('path/w-pawn.png', 'path/b-pawn.png')}"
    
    paths=get_matching_paths_recursively(source_folder,".png")
    paths=filter_paths(paths)
    random.shuffle(paths)    
    pieces={}
    for path in paths:
        # trim "w-" or "b-"
        key=path.split(os.sep)[-1][2:]
        if key not in pieces:
            pieces[key]=["",""]
        
        if os.sep+"w-" in path:
            pieces[key][0]=path
        elif os.sep+"b-" in path:
            pieces[key][1]=path
        else:
            print(f"Ignored '{path}'. Expected a PNG starting with 'w-' or 'b-'.")
    return pieces

def generate_pieces_preview_image():
    source_folder="pieces"
    target_folder="previews"
    width,height=1024,512
    column_count=12
    row_count=6
    cell_width=int(width/column_count)
    cell_height=int(height/row_count)
    
    tile_white=Image.open("board/tile-white1.png").convert("RGBA")
    tile_black=Image.open("board/tile-black1.png").convert("RGBA")
    tile_white=tile_white.resize((cell_width,cell_height),Image.ANTIALIAS)    
    tile_black=tile_black.resize((cell_width,cell_height),Image.ANTIALIAS)
    
    if not os.path.isdir(target_folder):
        os.mkdir(target_folder)
    
    preview_image=Image.new("RGBA",(width,height))
    
    pieces=get_pieces_dictionary(source_folder)
    
    index=0                
    for j in range(row_count):
        for i in range(column_count):
        
            if i%2==0:
                index+=1
                
            index%=len(pieces)            
            chosen=list(pieces.values())[index][i%2]
            
            piece_image=Image.open(chosen).convert("RGBA")
            piece_image=piece_image.resize((cell_width,cell_height),Image.ANTIALIAS)
            x=i*cell_width
            y=j*cell_height
            
            tile_image=tile_white if (i+j)%2==0 else tile_black
            preview_image.paste(tile_image,(x,y,x+cell_width,y+cell_height))
            
            preview_image.paste(piece_image,(x,y,x+cell_width,y+cell_height),piece_image)
            
    target=target_folder+os.sep+"pieces.png"
    preview_image.save(target)
    print(__file__ + f" wrote new file: '{target}'")

generate_pieces_preview_image()
